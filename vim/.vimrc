set nocompatible

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" Jump to any location specified by two characters
Plug 'justinmk/vim-sneak'

" A light and configurable statusline/tabline plugin for Vim
Plug 'itchyny/lightline.vim'

" Fuzzy finder
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'

" text manipulation
Plug 'FooSoft/vim-argwrap'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'

" Enable repeating supported plugin maps with "."
Plug 'tpope/vim-repeat'
Plug 'airblade/vim-rooter'

" theme
Plug 'morhetz/gruvbox'

call plug#end()



syntax enable
set title

set timeoutlen=300
set updatetime=150

set clipboard=unnamed
set formatoptions+=l
set list
set listchars=tab:>\ ,trail:-
set fillchars=vert:│
set history=500
set autowrite
set ignorecase
set smartcase
set showtabline=0
set number
set splitbelow
set wrap
set noeol
set backspace=2
set laststatus=2
set noshowmode

set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab

set undofile
set undodir=~/.cache/vim_undo

set incsearch
set hlsearch

set signcolumn=yes
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz

set termguicolors
set background=dark
colorscheme gruvbox

" https://github.com/vim/vim/issues/993
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"



""" plugins configuration
let g:fzf_command_prefix = 'Fzf'
let g:fzf_layout = { 'down': '~40%' }

let g:sneak#s_next = 1
let g:sneak#use_ic_scs = 1

let g:lightline = {
    \'colorscheme': 'gruvbox',
    \'active': {'right': []},
    \'component_expand': {},
    \'component_type': {'buffers': 'tabsel'},
    \'mode_map': {
        \'n' : 'N',
        \'i' : 'I',
        \'R' : 'R',
        \'v' : 'V',
        \'V' : 'V-LINE',
        \"\<C-v>": 'V-BLOCK',
        \'c' : 'C',
        \'s' : 'S',
        \'S' : 'S-LINE',
        \"\<C-s>": 'S-BLOCK',
        \'t': 'TERMINAL'
    \}
\}



""" bindings
let mapleader="\<space>"

tnoremap <esc> <c-\><c-n>
nnoremap <space> <nop>
inoremap <silent> jk <esc>
tmap <silent> jk <esc>

nnoremap <silent> <leader><leader> :noh<cr>

vnoremap <silent> . :normal .<cr>

nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap <c-d> <c-d>zz
nnoremap <c-u> <c-u>zz

nnoremap H ^
nnoremap L $

nnoremap Y y$

nnoremap j gj
nnoremap k gk

nnoremap > >>
nnoremap < <<
vnoremap > >gv
vnoremap < <gv

nnoremap <leader>ve :vs ~/.vimrc<cr>
nnoremap <leader>vi :w<cr> :PlugInstall<cr>
nnoremap <leader>vu :PlugUpdate<cr>

nnoremap <silent> <leader>qa :qa<cr>
nnoremap <silent> <leader>w :w<cr>
nnoremap <silent> <leader>qb :bdelete<cr>
nnoremap <silent> <leader>qc :cclose<cr>
nnoremap <silent> <leader>ql :lclose<cr>
nnoremap <silent> <leader>qp :pclose<cr>

nnoremap <silent> <c-p> :FzfFiles<cr>

nmap <leader>l <plug>NERDCommenterToggle
vmap <leader>l <plug>NERDCommenterToggle

nnoremap <silent> <leader>aw :ArgWrap<cr>

map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T



aug vimrc
    au!
    au BufWritePost .vimrc,init.vim nested source $MYVIMRC
aug END

aug filetype_vim
    au!
    au FileType vim let b:argwrap_line_prefix = '\'
aug END
