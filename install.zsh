#!/bin/zsh

export DOTFILES=$HOME/.dotfiles

if ! command -v antibody 1>/dev/null 2>&1; then
    echo ">>> install antibody"
    curl -sL git.io/antibody | BINDIR=$HOME/bin sh -s
fi

echo ">>> install plugins:"
cat $DOTFILES/zsh/plugins/plugins.txt
antibody bundle < $DOTFILES/zsh/plugins/plugins.txt > $DOTFILES/zsh/plugins/10_plugins.zsh

if [[ ! -d $HOME/.fzf ]]; then
    echo ">>> install fzf"
    git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
    $HOME/.fzf/install
fi

if [[ ! -d $HOME/.asdf ]]; then
    echo ">>> install asdf"
    git clone --depth 1 --branch v0.7.0 https://github.com/asdf-vm/asdf.git $HOME/.asdf
fi

create_symlink() {
    local origin=$1
    local target=$HOME/${origin##*/}

    if [ ! -f "$origin" ]; then
        echo "$origin not found"
        return 1
    fi

    if [ -f "$target" -a ! -L "$target" ]; then
        echo "backup target file: $target -> $target.backup"
        mv -f $target $target.backup
    fi

    echo "create symlink: $origin -> $target"
    ln -f -s $origin $target
}

echo ">>> create symlinks"
create_symlink $DOTFILES/tmux/.tmux.conf
create_symlink $DOTFILES/vim/.vimrc
create_symlink $DOTFILES/zsh/.zshrc

exec "$SHELL" -l
