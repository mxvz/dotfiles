#!/bin/zsh

if command -v minikube 1>/dev/null 2>&1; then
    if minikube status 1>/dev/null 2>&1; then
        eval $(minikube docker-env)
    fi
fi
