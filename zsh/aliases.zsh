globalias() {
   zle _expand_alias
   zle expand-word
   zle self-insert
}
zle -N globalias

unalias -a

alias -g G='| rg'
alias -g L='| bat'
alias -g h='--help | bat -plhelp'

alias rnds='openssl rand -hex 12'

alias st='open-git-origin-in-stash'

# modern unix
alias du='dust'
alias df='duf'
alias ls='lsd'
alias l='ls -lA -tr'
alias lt='ls --tree'
alias f='fd -uiL'

alias rf='rm -rf'
alias w1='watch -c -n1'
alias w5='watch -c -n5'
alias tm='tmux-attach-or-new'
alias tmux-save-pane='tmux capture-pane -pS -'
alias v='nvim'
alias dud='du -h -d 0'

# zsh
alias zs='exec "$SHELL" -l'
alias ze='vim $HOME/.dotfiles/zsh/.zshrc && zs'
alias za='vim $HOME/.dotfiles/zsh/aliases.zsh && zs'

# golang
alias gol='golangci-lint run'
alias goi='go install'
alias gog='go get -u -v'
alias got='go test'
alias gob='go build'
alias gobd='GOOS=darwin GOARCH=amd64 go build'
alias gobl='GOOS=linux GOARCH=amd64 go build'

# python
alias pil='pip list'
alias pii='pip install'
alias piu='pip install --upgrade'
alias pir='pip install -r requirements.txt'
alias pird='pip install -r dev-requirements.txt'
alias pif='pip freeze > requirements.txt'
alias pirmall='pip freeze --exclude-editable | xargs pip uninstall -y'
alias pea='pyenv activate $(pyenv virtualenvs --skip-aliases | fzf | tr -s " " | cut -d " " -f 2)'
alias ped='pyenv deactivate'
alias pel='pyenv virtualenvs --skip-aliases'

# git
alias s='git status -s'
alias g='tig --all'
alias d='git diff'
alias c='git checkout'
alias gf='git fetch'
alias gpl='git pull'
alias ga='git add'
alias gl='git log --oneline --graph --decorate --all'
alias gp='git push -u origin `git rev-parse --abbrev-ref HEAD`'
alias gp!='git push --force'
alias gc='git-commit-advanced'
alias gc!='git-commit-advanced --amend'
alias gca='git-commit-advanced `git-commit-auto-message`'
alias cm='git checkout master'
alias cn='git checkout -b'
alias cb='git checkout $(git branch | fzf)'
alias gr='git remote -v'
alias grt='git reset'
alias grth='git reset --hard'
alias grts='git reset --soft'
alias grta='git reset --hard && git clean -df'
alias gcl='git clone'
alias gb='git branch'
alias grb='git rebase'
alias grbc='git rebase --continue'
alias grba='git rebase --abort'
alias gdc='git diff --cached'
alias gh='git stash'
alias ghp='git stash pop'
alias ghl='git stash list'
alias ghd='git stash show -p'
alias ghb='git stash branch'
alias gsma='git submodule add'
alias gsmi='git submodule init'
alias gsmu='git submodule update'
alias gpf='ga . && gca && gp'
alias reset-author='git commit --amend --reset-author'
alias set-author='git config user.name "Maxim Verzun" && git config user.email "maxim.verzun@gmail.com"'

# docker
alias dk='docker'
alias dc='docker-compose'
alias ds='docker stack'
alias dps='docker ps -a'
alias dil='docker image list'
alias dir='docker image remove --force'
alias drm='docker rm --force'
alias drme='docker rm $(docker ps -a -q --no-trunc -f status=exited)'
alias drma='docker rm --force $(docker ps -a -q)'
alias db='docker build -t'
alias dr='docker run -it --rm'
alias dsys='docker system'

# vagrant
alias vg='vagrant'
alias vgu='vagrant up'

# minikube
alias mk='minikube'
alias mks='minikube status'
alias mkr='minikube start'
alias mkt='minikube delete'

# kubernetes
alias -g koj='-o json | jless'
alias -g koy='-o yaml | jless --yaml'
alias -g kst='--sort-by=.metadata.creationTimestamp | tac'
alias -g kan='--all-namespaces'
alias -g kxn='--context=naive-camel'
alias -g kxg='--context=great-way'
alias -g kxb='--context=blissful-nature'
alias -g kxh='--context=hopeful-music'
alias -g kxz='--context=zeta'
alias -g kxd='--context=deep-forest'
alias -g kxs='--context=fast-star'
alias -g kxc='--context=competent-tree'
alias -g kxf='--context=festive-history'
alias -g kxp='--context=busy-pillar'
alias -g kxo='--context=dazzling-owl'
alias -g kxr='--context=frosty-dot'
alias -g kxpr='--context=frosty-dot,dazzling-owl,busy-pillar,deep-forest,great-way'
alias -g kxst='--context=competent-tree,fast-star'

alias kcu='kubectl config unset current-context'
alias kcx='kubectx'
alias kcn='kubens'

alias k='kubectl'
alias kk='kubectl kustomize'
alias kg='kubectl get'
alias ka='kubectl apply'
alias kc='kubectl create'
alias kt='kubectl delete'
alias kd='kubectl describe'
alias kl='kubectl logs -f --tail=100'
alias kls="stern -t  -s 10m -c '(service|worker)'"
alias kpd="kubectl get pod -o=\"custom-columns=APP:.metadata.labels['app'],POD:.metadata.name,CONTAINERS:.spec.containers[*].name,INIT-CONTAINERS:.spec.initContainers[*].name\" --context=zeta -n pipeline"
alias kga='kubectl get deploy,po,svc,ing'
alias kgac='kubectl get deploy,po,svc,ing,cm,nx,crl,rls,pdb,destinationrule,peerauthentication,virtualservice,sidecar,envoyfilter'
alias ke='kubectl exec -it'
alias kg-rls='mkubectl get rls -o go-template-file=/Users/msverzun/.dotfiles/k8s/tpls/jibe_releases.tpl --context=dazzling-owl,deep-forest,busy-pillar,great-way -n '
alias kg-pod='mkubectl get pod -o go-template-file=/Users/msverzun/.dotfiles/k8s/tpls/service_pods.tpl --context=dazzling-owl,deep-forest,busy-pillar,great-way -n '
alias kg-tai='mkubectl get no -o go-template-file=/Users/msverzun/.dotfiles/k8s/tpls/node_with_taints.tpl'

# helm
alias hed='helm delete --purge'
alias hedf='helm delete --purge `helm list -q | fzf`'
alias hel='helm list'

# kafka
alias kkcons='kafka-console-consumer.sh --bootstrap-server "${KAFKA_ADDR}" --topic'
alias kkprod='kafka-console-producer.sh --broker-list "${KAFKA_ADDR}" --topic'
alias kktls='kafka-topics.sh --zookeeper "${ZK_ADDR}" --list'
alias kktde='kafka-topics.sh --zookeeper "${ZK_ADDR}" --describe'
alias kktrm='kafka-topics.sh --zookeeper "${ZK_ADDR}" --delete --topic'
alias kktmk='kafka-topics.sh --zookeeper "${ZK_ADDR}" --create --topic'
alias kkg='kafka-consumer-groups.sh --bootstrap-server ${KAFKA_ADDR}'
alias kkgls='kafka-consumer-groups.sh --bootstrap-server ${KAFKA_ADDR} --list'
alias kkgde='kafka-consumer-groups.sh --bootstrap-server ${KAFKA_ADDR} --describe --group'

# jira id from branch name
alias -g jid='`git rev-parse --abbrev-ref HEAD | sed -E "s/([A-Z]{1,8}-[0-9]{1,6}).*/\1/"`'

alias cht='() { curl cheat.sh/$1 }'

alias noize-stardust='open "https://mynoise.net/NoiseMachines/stardustDroneGenerator.php?c=0&l=3637374847473827222400&d=0"'
alias noize-pebble='open "https://mynoise.net/NoiseMachines/pebbleBeachNoiseGenerator.php?c=0&l=3343654965644630300500&d=0"'

alias color-palette='for i in {0..255}; do printf "\x1b[48;5;${i}m     \x1b[0m colour${i}\n"; done'


# other
alias reload-icloud='killall bird'
alias quar='xattr -r -d com.apple.quarantine'


alias s3argo='s3cmd -c /Users/msverzun/.s3cfg_argowf_paas du -H s3://argowf-paas-default/'
alias s3argost='s3cmd -c /Users/msverzun/.s3cfg_argowf_paas_staging du -H s3://argowf-paas-default-staging/'
