#!/bin/zsh

export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND="bg=9,fg="
export HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND="bg=10,fg="

autoload -Uz manydots-magic
manydots-magic
