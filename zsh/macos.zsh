#!/bin/zsh

export HOMEBREW_NO_AUTO_UPDATE=1

defaults write NSGlobalDomain KeyRepeat -int 2
defaults write NSGlobalDomain InitialKeyRepeat -int 15
