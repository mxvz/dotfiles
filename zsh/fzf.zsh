#!/bin/zsh

export FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --exclude .git'
export FZF_DEFAULT_OPTS='--height 40% --border --inline-info --reverse'
export FZF_TMUX=1


