#!/bin/zsh

export PYENV_ROOT=$HOME/.pyenv
path+=$PYENV_ROOT/bin


if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init --path)"
    eval "$(pyenv init -)"
fi
