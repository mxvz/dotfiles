#!/bin/zsh

autoload -Uz compinit
if [ $(date +'%j') != $(/usr/bin/stat -f '%Sm' -t '%j' ${ZDOTDIR:-$HOME}/.zcompdump) ]; then
  compinit
else
  compinit -C
fi


fpath=($DOTFILES/zsh/functions $fpath)
autoload -Uz "$DOTFILES"/zsh/functions/*(:t)


for file in $DOTFILES/zsh/plugins/*.zsh; do
  source $file
done
