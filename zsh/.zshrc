#!/bin/zsh

# zmodload zsh/zprof

typeset -U path fpath manpath

export DOTFILES=$HOME/.dotfiles

export EDITOR='vim'
export GUITOR='code'

for file in $DOTFILES/zsh/*.zsh; do
  source $file
done

# zprof

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
