# reference
#
# useful links:
# http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html
# http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#Version-Control-Information
#
# git:
# %b => current branch
# %a => current action (rebase/merge)
#
# prompt:
# %F => color dict
# %f => reset color
# %~ => current path
# %* => time
# %n => username
# %m => shortname host
# %(?..) => prompt conditional - %(condition.true.false)
#
# for inspiration:
# - https://github.com/chauncey-garrett/zsh-prompt-garrett
# - https://github.com/miekg/lean
# - https://github.com/sindresorhus/pure

PROMPT_BESPOKE_KUBE_CTX=""

prompt_bespoke_git_dirty() {
    command git rev-parse --is-inside-work-tree &>/dev/null || return
    command test -n "$(command git status --porcelain --ignore-submodules -uno 2>/dev/null | head -1)"
    (($? == 0)) && echo '*'
}

prompt_bespoke_cmd_exec_time() {
    local stop=$EPOCHSECONDS
    local start=${cmd_start_timestamp:-$stop}
    integer elapsed=$stop-$start
    (($elapsed > 5)) && pretty-time $elapsed
}

prompt_bespoke_pwd() {
    local current_path="`print -Pn '%2/'`"
    if (($#current_path / $COLUMNS.0 > 0.1)); then
        current_path="`print -Pn '%1/'`"
    fi
    print "$current_path"
}

prompt_bespoke_get_tool_version() {
    exec cat $HOME/.tool-versions | grep $1 | cut -d' ' -f2
}

prompt_bespoke_preexec() {
    cmd_start_timestamp=$EPOCHSECONDS
}

prompt_bespoke_main_precmd() {
    vcs_info
    rehash
    setopt promptsubst

    exec_time=$(prompt_bespoke_cmd_exec_time)
    [ ! -z "$exec_time" ] && exec_time="$exec_time "

    pyenv_name=$PYENV_VERSION
    [ ! -z "$pyenv_name" ] && pyenv_name="$PYENV_VERSION "

    PROMPT=""
    PROMPT+="%F{yellow}$exec_time%f"
    PROMPT+="%F{gray}"${pyenv_name:t}"%f"
    PROMPT+="%F{8}`date '+%X'`%f "
    PROMPT+="$prompt_bespoke_ssh_host"
    PROMPT+="%(1j.%F{yellow}[%j]%f .)"
    PROMPT+="%F{blue}`prompt_bespoke_pwd`%f"
    PROMPT+="%F{yellow}$vcs_info_msg_0_`prompt_bespoke_git_dirty`%f"
    PROMPT+="%F{8}"${PROMPT_BESPOKE_KUBE_CTX}"%f"
    PROMPT+=" %(?.%F{green}.%F{red})❯%f "

    RPROMPT=""

    unset cmd_start_timestamp # reset value since `preexec` isn't always triggered
}

prompt_bespoke_kubectl_precmd() {
    local kubeconfig updated_at kubeconfig_stat context

    kubeconfig="$HOME/.kube/config"
    if [[ -n "$KUBECONFIG" ]]; then
        kubeconfig="$KUBECONFIG"
    fi

    if ! kubeconfig_stat="$(stat -L -c%y "$kubeconfig" 2>/dev/null)"; then
        PROMPT_BESPOKE_KUBE_CTX=" kubeconfig?"
        return 1
    fi

    zstyle -s ':zsh-kubectl-prompt:' updated_at updated_at
    if [[ "$updated_at" == "$kubeconfig_stat" ]]; then
        return 0
    fi
    zstyle ':zsh-kubectl-prompt:' updated_at "$kubeconfig_stat"

    ctx=$(command kubectl config view --minify -o json 2>/dev/null | jq -r '{cluster: .contexts[0].context.cluster, ns: .contexts[0].context.namespace} | if .ns == "default" then "\(.cluster)" else "\(.cluster):\(.ns)" end')

    if [[ -n "$ctx" ]]; then
        PROMPT_BESPOKE_KUBE_CTX=" $ctx"
    fi
    
    return 0
}

prompt_bespoke_setup() {
    # http://zsh.sourceforge.net/Doc/Release/Parameters.html
	export PROMPT_EOL_MARK=''

    prompt_opts=(cr percent sp subst)

    zmodload zsh/datetime
    autoload -Uz add-zsh-hook
    autoload -Uz vcs_info

    add-zsh-hook precmd prompt_bespoke_kubectl_precmd
    add-zsh-hook precmd prompt_bespoke_main_precmd
    add-zsh-hook preexec prompt_bespoke_preexec

    zstyle ':zsh-kubectl-prompt:' updated_at ""

    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:git*' formats ' %b'
    zstyle ':vcs_info:git*' actionformats ' %b|%a'

    [[ "$SSH_CONNECTION" != '' ]] && prompt_bespoke_ssh_host="%F{green}%m%f "

    return 0
}

prompt_bespoke_setup "$@"
