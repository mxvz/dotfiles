#!/bin/zsh

bindkey -e
bindkey "^K" history-substring-search-up
bindkey "^J" history-substring-search-down
bindkey "^E" forward-word
bindkey "^W" backward-word

bindkey " " globalias
bindkey -M isearch " " magic-space
