#!/bin/zsh

manpath=('/usr/local/opt/coreutils/libexec/gnuman' $manpath)


path=('/usr/local/opt/coreutils/libexec/gnubin' $path)
path=('/usr/local/opt/node@12/bin' $path)


path+=$HOME/bin
path+=$HOME/.local/bin
path+=/usr/local/kubebuilder/bin
path+=$HOME/.krew/bin
path+=$HOME/.fig/bin
path+=$HOME/.docker/bin

export CHEAT_CONFIG_PATH="~/.dotfiles/cheat/conf.yml"

export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
