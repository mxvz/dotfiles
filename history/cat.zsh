#!/bin/zsh

export LC_ALL=C

cat $HOME/.zsh_history |\
    awk -F';' '{print $2}' |\
    # grep "tcpdump" |\
    grep "kubectl" |\
    ./clustering.py