#!/usr/bin/env python3

import sys
from fuzzywuzzy import fuzz

lines = sys.stdin.readlines()

groups = list()
for line in lines:
    for group in groups:
        if all(fuzz.ratio(line, l) > 50 for l in group):
            group.append(line)
            break
    else:
        groups.append([line, ])

for group in groups:
    print('-'*30)
    for line in group:
        print(line.strip('\n'))