{{- define "taints" -}}
    {{- if $taint := (index .spec "taints") }}
        {{- range $taint }}
            {{- .key }}={{ .value }}:{{ .effect }}{{ ";" }}
        {{- end }}
    {{- else -}}
        <no taints>
    {{- end}}
{{- end -}}
{{- define "Ready" -}}
    {{- range .status.conditions -}}
        {{- if eq .type "Ready" -}}
            {{- if eq .status "True" -}}
                {{- printf "%-10s " "Ready" -}}
            {{- else -}}
                {{- printf "%-10s " "NotReady" -}}
            {{- end -}}
        {{- end -}}
    {{- end -}}
{{- end -}}
{{printf "%-30s %-10s %s\n" "Node" "Status" "Taints"}}
{{- range .items}}
    {{- printf "%-30s " .metadata.name}}
    {{- template "Ready" . -}}
    {{- template "taints" . -}}
    {{- "\n" }}
{{- end}}