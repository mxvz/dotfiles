
{{print "App" "Pod" "Containers" "\n"}}
{{- range .items}}
    {{- print (index .metadata.labels "app")}}
    {{- print .metadata.name}}
    {{- range .spec.containers -}}
        {{.name}}{{":"}}{{ .image }}{{"| "}}
    {{- end -}}
    {{- "\n" }}
{{- end}}