{{printf "%-30s %s %s\n" "Name" "Version" "Status"}}
{{- range .items}}
    {{- printf "%-30s " .spec.name}}
    {{- printf .spec.version}}
    {{- print .spec.status}}
    {{- "\n" }}
{{- end}}