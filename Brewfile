tap "boz/repo"
tap "homebrew/bundle"
tap "homebrew/cask"
tap "homebrew/cask-versions"
tap "homebrew/core"
tap "homebrew/services"
tap "minio/stable"
tap "mongodb/brew"
# Simple, modern, secure file encryption
brew "age"
# Terminal bandwidth utilization tool
brew "bandwhich"
# Bourne-Again SHell, a UNIX command interpreter
brew "bash"
# Programmable completion for Bash 3.2
brew "bash-completion"
# Clone of cat(1) with syntax highlighting and Git integration
brew "bat"
# High performance key/value database
brew "berkeley-db", link: true
# Yet another cross-platform graphical process/system monitor
brew "bottom"
# Freely available high-quality data compressor
brew "bzip2"
# GNU internationalization (i18n) and localization (l10n) library
brew "gettext"
# Core application library for C
brew "glib"
# Manage compile and link flags for libraries
brew "pkg-config"
# GNU File, Shell, and Text utilities
brew "coreutils"
# Open source suite of directory software
brew "openldap"
# Get a file from an HTTP, HTTPS or FTP server
brew "curl"
# Disk Usage/Free Utility - a better 'df' alternative
brew "duf"
# More intuitive version of du in rust
brew "dust"
# Command-line tool to interact with exercism.io
brew "exercism"
# Simple, fast and user-friendly alternative to find
brew "fd"
# User-friendly command-line shell for UNIX-like operating systems
brew "fish"
# Distributed revision control system
brew "git"
# Syntax-highlighting pager for git and diff output
brew "git-delta"
# Git extension for versioning large files
brew "git-lfs"
# GNU implementation of the famous stream editor
brew "gnu-sed"
# Validating, recursive, caching DNS resolver
brew "unbound"
# GNU Transport Layer Security (TLS) Library
brew "gnutls"
# GNU grep, egrep and fgrep
brew "grep"
# Command-line tool for generating regular expressions
brew "grex"
# Improved top (interactive process viewer)
brew "htop"
# Website copier/offline browser
brew "httrack"
# Command-line benchmarking tool
brew "hyperfine"
# Lightweight and flexible command-line JSON processor
brew "jq"
# Kubernetes CLI To Manage Your Clusters In Style!
brew "k9s"
# Package manager for kubectl plugins
brew "krew"
# Terminal file manager
brew "lf"
# C library of Git core methods that is re-entrant and linkable
brew "libgit2"
# Clone of ls with colorful output, file type icons, and more
brew "lsd"
# Development kit for the Java programming language
brew "openjdk"
# Python package management tool
brew "poetry"
# Modern replacement for ps written by Rust
brew "procs"
# Python version management
brew "pyenv"
# Pyenv plugin to manage virtualenv
brew "pyenv-virtualenv"
# Interpreted, interactive, object-oriented programming language
brew "python@3.10"
# Interpreted, interactive, object-oriented programming language
brew "python@3.9"
# Reattach process (e.g., tmux) to background
brew "reattach-to-user-namespace"
# Search tool like grep and The Silver Searcher
brew "ripgrep"
# Powerful, clean, object-oriented scripting language
brew "ruby"
# Fast and accurate code counter with complexity and COCOMO estimates
brew "scc"
# Static analysis and lint tool, for (ba)sh scripts
brew "shellcheck"
# Add a public key to a remote machine's authorized_keys file
brew "ssh-copy-id"
# Tail multiple Kubernetes pods & their containers
brew "stern"
# Traceroute implementation using TCP packets
brew "tcptraceroute"
# User interface to the TELNET protocol
brew "telnet"
# Text interface for Git repositories
brew "tig"
# Terminal multiplexer
brew "tmux"
# Vi 'workalike' with many additional features
brew "vim"
# Executes a program periodically, showing output fullscreen
brew "watch"
# Internet file retriever
brew "wget"
# Process YAML documents from the CLI
brew "yq"
# General-purpose lossless data-compression library
brew "zlib"
# Shell extension to navigate your filesystem faster
brew "zoxide"
# UNIX shell (command interpreter)
brew "zsh"
# MinIO Client for object storage and filesystems
brew "minio/stable/mc"
# Unpacks archive files
cask "the-unarchiver"
